// hamburger
    const hamburger = document.querySelector(".hamburger");
    const navmenu = document.querySelector(".menu");

    hamburger.addEventListener("click",() => {
        hamburger.classList.toggle("active");
        navmenu.classList.toggle("active");
    })

    document.querySelectorAll(".menu").forEach( n => n.addEventListener("click", () => {
        hamburger.classList.remove("active");
        navmenu.classList.remove("active")
    }))

// login

   const container = document.querySelector(".container");
        signup = document.querySelector(".signup-text");
        login = document.querySelector(".login-link");

        signup.addEventListener("click", ( ) =>{
            container.classList.add("active");
        });
        login.addEventListener("click", ( ) => {
            container.classList.remove("active")
        });

// swiper
$(".slider").owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 2000, //2000ms = 2s;
    autoplayHoverPause: true,
    spaceBetween:10,
    });