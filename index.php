<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/curd.css">
    <title>Upload Buku</title>
</head>
<body>
    <h1>Data Buku</h1>
    <a class="buku" href="tambah_buku.php">+ &nbsp; Tambah Buku</a>
    <table border="">
        <thead>
            <tr>
                <th>No.</th>
                <th>Judul</th>
                <th>Gambar</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            require_once 'koneksi.php';
            $query = "SELECT * FROM upload";
            $result = mysqli_query ($koneksi, $query);

            if(!$result){
                die("Querry Error :" .mysqli_errno($koneksi). " - " .mysqli_error($koneksi));
            }
            $no = 1;
            
            while ($row = mysqli_fetch_assoc($result)){
            ?>
            <tr>
                <td style="text-align:center ;"><?php echo $no?></td>
                <td style="text-align:center ;"><?php echo $row['Judul']?></td>
                <td style="text-align:center ;"><img src="gambar/<?php echo $row['Gambar']?>"</td>
                <td style="text-align:center ;">
                    <a href="edit_buku.php?id=<?php echo $row['Id'];?>">edit</a>
                    <a href="hapus_buku.php?id=<?php echo $row['Id'];?>" onclick="return confirm('Anda yakin hapus data ini?')">hapus</a>
                </td>
            </tr>
            <?php
            $no++;
            }
            ?>
        </tbody>
    </table>
</html>